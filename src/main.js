import Vue from 'vue';
import Greetings from './components/Greetings.vue';
import Suggestions from './components/Suggestions.vue';
import AirportSelection from './components/AirportSelection.vue';
import App from './App.vue';
import ApolloClient from 'apollo-boost';

import KeenUI from 'keen-ui';
import 'keen-ui/dist/keen-ui.css';

import VueApollo from 'vue-apollo';
import VueResource from 'vue-resource';
import VueRouter from 'vue-router';

Vue.config.productionTip = false;
Vue.use(VueResource);
Vue.use(VueRouter);
Vue.use(VueApollo);
Vue.use(KeenUI);

const apolloClient = new ApolloClient({
  uri: 'https://api.graphcms.com/simple/v1/awesomeTalksClone',
});

const localClient = new ApolloClient({
  uri: `${process.env.VUE_APP_API_URL}/graphql`,
  fetchOptions: {},
});

const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
  clients: {
    local: localClient,
  },
});

const routes = [
  { path: '/', component: Greetings },
  { path: '/suggestions', component: Suggestions },
  { path: '/where', component: AirportSelection },
];

const router = new VueRouter({ routes, mode: 'history' });
new Vue({
  apolloProvider,
  data: {
    selectedSeasonIndex: null,
    selectedIataCode: '',
    depatureIataCode: '',
  },
  el: '#root',
  render: h => h(App),
  router,
});
