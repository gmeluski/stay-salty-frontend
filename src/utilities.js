export const getSeasonRange = seasonIndex => {
  let seasonRange = [];
  let leadMonth;

  if (seasonIndex === 0) {
    seasonRange.push(12);
    leadMonth = seasonIndex;
  } else {
    leadMonth = seasonIndex + seasonIndex * 2;
    seasonRange.push(leadMonth);
  }

  for (let i = 0; i < 2; i++) {
    leadMonth++;
    seasonRange.push(leadMonth);
  }

  return seasonRange;
};
