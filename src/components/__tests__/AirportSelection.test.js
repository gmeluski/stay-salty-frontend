import { mount, shallowMount } from '@vue/test-utils';
import { getSeasonRange } from '../../utilities';
import AirportSelection, {
  getDepartureDate,
  STAY_LENGTH,
} from '../AirportSelection';
import moment from 'moment';

describe('AirportSelection component', () => {
  it('it is an instance', () => {
    const wrapper = mount(AirportSelection);
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('returns a date five days from the current date', () => {
    const wrapper = mount(AirportSelection);
    const LEAVE_DATE = '01';
    const currentDate = `19-10-${LEAVE_DATE}`;
    const RETURN_DATE = `0${parseInt(LEAVE_DATE, 10) + STAY_LENGTH}`;

    expect(wrapper.vm.handleDate(currentDate)).toEqual(`19-10-${RETURN_DATE}`);
  });
});

describe('finding the next travel date', () => {
  const WINTER_INDEX = 0;
  const SPRING_INDEX = 1;
  const FALL_INDEX = 3;
  const currentMonth = 10;

  it('returns the nearest month next year if all months are smaller than the current month', () => {
    const wrapper = shallowMount(AirportSelection);
    const seasonRange = getSeasonRange(SPRING_INDEX);
    const outcome = `${moment()
      .add(1, 'years')
      .format('YY')}-${seasonRange[0]}-01`;
    expect(wrapper.vm.getDepartureDate(SPRING_INDEX, currentMonth)).toEqual(
      outcome,
    );
  });

  it('returns todays date if the selected current month is part of the desired travel range', () => {
    const wrapper = shallowMount(AirportSelection);
    const outcome = `${moment().format('YY')}-${currentMonth}-${moment().format(
      'DD',
    )}`;
    expect(wrapper.vm.getDepartureDate(FALL_INDEX, currentMonth)).toEqual(
      outcome,
    );
  });

  it('returns the nearest future month this year if the selected range has values bigger than the current months', () => {
    const wrapper = shallowMount(AirportSelection);
    const seasonRange = getSeasonRange(WINTER_INDEX);
    const outcome = `${moment().format('YY')}-${seasonRange[0]}-01`;
    expect(wrapper.vm.getDepartureDate(WINTER_INDEX, currentMonth)).toEqual(
      outcome,
    );
  });
});
