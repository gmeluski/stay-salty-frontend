import { getSeasonRange } from '../utilities';

describe('getting the season array by index', () => {
  it('returns correctly for winter', () => {
    const range = getSeasonRange(0);
    expect(range).toEqual([12, 1, 2]);
  });

  it('returns correctly for all other seasons', () => {
    expect(getSeasonRange(2)).toEqual([6, 7, 8]);
  });
});
