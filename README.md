# Stay Salty!

Currently stay salty is running in a microservices designed architecture. 
Right now there are three repos: the frontend, the backend, and the repo which manages
our sql migrations. To get started, you will have to clone all three to your local
environment.

## production environment setup
Make sure [yarn is installed](https://yarnpkg.com/lang/en/docs/install/#centos-stable)


### Compile and minifies for production
```
yarn run build
```

### Run the server for the client app
```
node index.js
```


## development environnment setup
Assumptions:
NVM installed to manage node

do this first:

```
nvm install v10.15.1 &&
nvm alias default v10.15.1 &&
nvm use v10.15.1 &&
yarn global add @vue/cli
```

run `vue --version`, you should get a response with the current version number


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```


### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Renewing Let's Encrypt in Production
```
sudo certbot renew
```

## Todo
- move `node index.js` into the `package.json` file as a script
- get yarn installed on the server
- create a .env for production
- securely manage environment configuration variables
- create config variables
- set up a separate API layers to contain third party endpoints
- add object spread option
